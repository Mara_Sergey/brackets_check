<?php

namespace App\Http\Controllers;

use Ds\Stack;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function stack(Request $request)
    {
        $text = $request->get('text');

        if ($this->pairingCheck($text)) {
            $status = 'ok';
        } else {
            $status = 'fail';
        }

        return response(['status' => $status]);
    }

    private function pairingCheck($string)
    {
        $stack = new Stack();
        $brackets = [
            '{',
            '(',
            '[',
            '}',
            ')',
            ']',
        ];

        for ($i = 0; $i < strlen($string); $i++) {
            $x = str_split($string)[$i];
            if ($x == '{' || $x == '(' || $x == '[') {
                $stack->push($x);
            } else {
                if (!$stack->isEmpty()) {
                    $ch = $stack->peek();
                    if ($ch == '{' && $x == '}') {
                        $stack->pop();
                    } else if ($ch == '(' && $x == ')') {
                        $stack->pop();
                    } else if ($ch == '[' && $x == ']') {
                        $stack->pop();
                    } else {
                        break;
                    }
                } else {
                    if (!in_array($x, $brackets)) {
                        continue;
                    } else {
                        $stack->push($x);
                        break;
                    }
                }
            }
        }

        return $stack->isEmpty();
    }

}
